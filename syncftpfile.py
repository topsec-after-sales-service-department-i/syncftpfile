#!/usr/bin/env python
# -*- encoding:utf-8 -*-
# @ModuleName: main
# @Function:
# @Author: RyneZ
# @Time: 2021/9/8 12:49

import csv
from datetime import datetime
import os
from sys import exit
import re
import xmltodict
import msvcrt
from ftplib import FTP
import argparse
import urllib.request
import urllib.error
import logging
from progressbar import Bar, ETA, FileTransferSpeed, Percentage, ProgressBar
#import multiprocessing.dummy as mp


class FTPSync:

    def __init__(self):
        try:
            logging.basicConfig(
                filename='LOGS',
                level=logging.DEBUG,
                format='%(asctime)s [%(levelname)s] %(name)s - %(message)s')
            self.logger = logging.getLogger(__name__)
            with open('config.xml', encoding='utf-8') as configfile:
                self.xmlcontentstr = configfile.read()
            self.serverdict = xmltodict.parse(self.xmlcontentstr)
            self.ipaddr = self.serverdict['cfg']['ipaddr']
            self.port = self.serverdict['cfg']['port']
            self.user = self.serverdict['cfg']['user']
            self.passwd = self.serverdict['cfg']['pass']
            self.encoding = self.serverdict['cfg']['encoding']
            self.dir = self.serverdict['cfg']['localdir']
            self.ftp = FTP()
            self.files = []
            self.infos = []
        except Exception as E:
            self.logger.exception(
                "ftp server information config file not found")
            input("读取配置文件:config.xml失败，程序将退出，请检查文件是否存在或者是否符合格式要求\n请按回车键退出...")
            exit(-1)

    def ftpConnect(self):

        print('*正在连接ftp服务器...')
        self.logger.info("start to connect ftp server")
        try:
            self.ftp.encoding = self.encoding
            self.ftp.connect(self.ipaddr, int(self.port))
            self.ftp.login(self.user, self.passwd)
        except Exception as E:
            self.logger.exception("connectting ftp server failed")
            input("连接FTP服务器失败，程序将退出\n请按回车键退出...")
            exit(-1)

    def getFile(self):
        try:
            with open('ftpserverconfig.csv') as f:
                featureinfos = csv.reader(f)
                next(featureinfos)  # 排除掉第一行
                self.infos = []  # 用来存储当前目录下的文件/文件夹列表
                print(
                    '*开始获取新版本规则库文件列表，请稍等...\n')
                for featureinfo in featureinfos:
                    filename = self.getFileName(featureinfo)
                    if filename == '':
                        continue
                    self.files.append((filename, featureinfo))
                    self.infos = []
                self.logger.info("get remote file's path success")
                return self.files
        except FileNotFoundError as E:
            self.logger.exception("config file not found")
            input(
                "读取服务器配置文件:ftpserverconfig.csv失败，程序将退出，请检查文件是否存在或者内容是否符合格式要求\n请按回车键退出...")
            exit(-1)
        except Exception as E:
            self.logger.exception("an unknown error occurred")
            input("未知错误...\n请按回车键退出...")
            exit(-1)

    # def downFile(self, num):
    def downFile(self, files, featureinfo):
        #files, featureinfo=self.files[num]
        try:
            if files[0][0].startswith('TopScanner') or files[0][0].startswith(
                    'sig'):  # 如果类似topscanner是包含在文件夹中的，无法下载文件夹，需要特殊处理
                topscannerlist = []
                self.ftp.dir(
                    ''.join([featureinfo[1], files[0][0]]), topscannerlist.append)
                topscannerlist.pop(0)
                for file in topscannerlist:
                    if file.split()[8].startswith(
                            'NPScanner') or file.split()[8].startswith('sig'):
                        filename = files[0][0] + '/' + file.split()[8]
                serverFile = ''.join([featureinfo[1], filename])
                localFile = ''.join(
                    [self.dir, featureinfo[0], '/', filename.split('/')[-1]])
            else:
                serverFile = ''.join([featureinfo[1], files[0][0]])
                localFile = ''.join(
                    [self.dir, featureinfo[0], '/', files[0][0].split('/')[-1]])
            localDir = ''.join([self.dir, featureinfo[0]])

            print('--------------------')
            print('规则库文件将要保存至：' + localFile)
            if os.path.exists(localFile):
                print("当前本地规则库文件已是最新版本！")
                return 1
            print('即将从FTP服务器下载：' + serverFile)
            if not os.path.exists(localDir):
                os.makedirs(localDir)
            file = open(localFile, 'wb')
            size = self.ftp.size(serverFile)  # 进度条参数
            widgets = ['Downloading: ', Percentage(), ' ',
                       Bar(marker='#', left='[', right=']'),
                       ' ', ETA(), ' ', FileTransferSpeed()]
            progessBar = ProgressBar(widgets=widgets, maxval=size).start()
            bufsize = 1024
            datelen = 0

            def file_write(data):  # 在ftp下载函数中调用
                nonlocal datelen
                datelen = datelen + len(data)
                file.write(data)
                nonlocal progessBar
                progessBar.update(datelen)

            self.ftp.retrbinary("RETR " + serverFile, file_write, bufsize)
            file.close()
            progessBar.finish()
            self.logger.info("sync " + serverFile + ' success')
        except KeyboardInterrupt:
            if localFile:
                print('正在删除当前未完成的文件\n')
                self.logger.error("unexpect exit in downFile()")
                file.close()
                os.remove(localFile)
                self.logger.info("delete " + localFile + ' success')
                input('已删除' + localFile + '\n请按回车键退出...')
                exit(-1)
            exit(0)
        except Exception as E:
            self.logger.exception("an unknown error occurred")
            print('下载出错了，提示错误信息为：' + repr(E))

    def getFileName(self, featureinfo):
        try:
            indexrecord = {}
            serverDIR = featureinfo[1]
            fileNameReg = re.compile(featureinfo[2])
            self.ftp.dir(serverDIR, self.infos.append)
            self.infos.pop(0)  # 去除第一条无用信息
        except KeyboardInterrupt:
            self.logger.error("unexpect exit in getFileName()")
            input('请按回车键退出...')
            exit(-1)
        except IndexError:
            self.logger.error("当前行未匹配到内容"+featureinfo[0])
            print("当前行未匹配到内容"+featureinfo[0])
            return ''
        except Exception as E:
            self.logger.exception("config file error:"+repr(E))
            print('当前处理出错，请检查配置文件对应行是否有错误,可能为服务器端目录有变化或者文件名异常：\n' + ','.join(featureinfo))
            return ''
        for info in self.infos:
            filename = info.split()[8]  # 当前目录下文件或文件夹名
            matchName = re.match(fileNameReg, filename)

            if matchName:
                try:  # 后续逻辑或许有点乱**代码还需修改**
                    indexrecord[filename] = datetime.strptime(
                        matchName.group(1), '%Y.%m.%d').date()
                except ValueError:
                    try:
                        indexrecord[filename] = datetime.strptime(
                            matchName.group(1), '%Y%m%d').date()
                    except ValueError:
                        try:  # 能运行到此就意味着匹配到的数据不是以日期为格式的了，需要特殊处理
                            if matchName.group(
                                    1) == 'ISP-IP-LISTS.bin':  # ISP列表直接下载即可
                                indexrecord[filename] = 'ISP-IP-LISTS.bin'
                            else:
                                nums = matchName.group(1).split('.')
                                # print(nums)
                                num = 0
                                for i in range(
                                        len(nums)):  # 将x.x.x.x这种形式的版本号处理为可以比较大小的方式
                                    num = num + int(nums[i]) * \
                                        1000000 / (10 ** (i * 2))
                                indexrecord[filename] = num
                                # indexrecord[filename] = datetime.strptime(matchName.group(1), '%H.%I.%M.%S').date()
                        except ValueError as E:
                            self.logger.exception(
                                "unexpect error in getFileName()")
                            print('当前匹配：', str(E))
                        except IndexError as E:
                            self.logger.exception(
                                "unable to match latest update file")
                            print('正则没有匹配到内容', repr(E))
        dates = sorted(
            indexrecord.items(),
            key=lambda x: x[1],
            reverse=True)  # 根据时间排序
        return dates

    def close(self):
        """
        正常退出
        :return:
        """
        self.logger.info("program finish")
        self.ftp.quit()
        input("请按回车键退出...")
        exit(0)


def update():
    url = 'https://gitlab.com/rynez/ftpfilesync/-/archive/main/ftpfilesync-main.zip'
    print('开始下载到当前目录...请稍候')
    try:
        f = urllib.request.urlopen(url)
        with open("ftpfilesync.zip", "wb") as code:
            code.write(f.read())
        print('下载完成，解压目录下ftpfilesync.zip覆盖原文件即可。')
    except urllib.error.HTTPError:
        print('下载地址出错，请及时反馈...')
    except urllib.error.URLError:
        print('更新通过访问https://gitlab.com下载压缩包，请检查DNS或者网络连接情况')
    except PermissionError:
        print('写入文件失败，请检查目录权限或者目录下ftpfilesync.zip是否处于打开状态')


def main():
    #os.system('mode con:cols=125 lines=42')
    print('''
+---------------------------------------------------------------------------------------------------------------------+
                                           .
                            .....1111000111.  说明:
                   ...111100000000001...          本程序用以自动从ftp服务器下载最新版本的规则库，ftp登录信息在程序同
             ..11110101010001111..            目录下config.xml中定义，默认使用内网地址，请连接VPN后再开始下载。
        ..1111011111011111.        .11            ftpserverconfig.csv文件中定义不同的设备规则库从ftp上哪个目录下载，
      .0001111111011..            10001       对下载后的文件不清楚可以在文件中查看简单的说明，如有不需要下载的文件也可
      .00111111..               100001        以删除整行，但目前不建议对文件名格式进行修改。
       .111.                  10001.              下载过程中需要注意的事项：
                             0001                 1.其他原因导致程序中止运行请记得根据提示目录手动删除未下载完成的文件
                            000.                  2.按下CTRL+C可停止下载并自动删除当前未完成的文件
                           000
                          0000
                         .0001                **在当前窗口中鼠标左键任意点击将会导致下载暂停，按回车继续
                         .0000.
                         .00000.
                          1010001
                           1011000.
                           .0110001               当前版本：1.0.4
                          .0000000.
                        .000000001
                      .0000011.
                    .101.
                   ..
                  .

+---------------------------------------------------------------------------------------------------------------------+\n
        ''')
    print("按任意键开始...\n")
    ord(msvcrt.getch())
    # 从excel中获取设备信息
    ftpcon = FTPSync()
    ftpcon.ftpConnect()
    files = ftpcon.getFile()
    for file in files:
        ftpcon.downFile(file[0], file[1])
    ftpcon.close()


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description='默认直接执行无需附带参数\n更新程序： syncftpfile -update')
    parser.add_argument(
        '-update',
        action='store_true',
        help='执行更新程序update.exe')
    isupdte = parser.parse_args().update

    if isupdte:
        update()
    else:
        main()
